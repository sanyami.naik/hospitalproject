package com.Hospital.Management.System.controller;

import com.Hospital.Management.System.Dto.AssignDto;
import com.Hospital.Management.System.entity.*;
import com.Hospital.Management.System.service.AdminService;
import com.Hospital.Management.System.service.AssignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin
public class AdminController {

    @Autowired
    AssignService assignService;

    @Autowired
    AdminService adminService;

    @PostMapping("/assignDoctor")
    public String assignDoctor(@RequestBody AssignDto assignDto)
    {
        return assignService.assignDoctor(assignDto);


    }

    @PostMapping("/assignNurse")
    public String assignNurse(@RequestBody AssignDto assignDto)
    {
        return assignService.assignNurse(assignDto);

    }



    @GetMapping("/showNurses")
    public List<Nurse> showAllNurses(){
        return this.adminService.showAllNurses();
    }


    @GetMapping("/showDoctors")
    public List<Doctor> showAllDoctors(){
        return this.adminService.showAllDoctors();
    }
//



}
