package com.Hospital.Management.System.controller;


import com.Hospital.Management.System.Dto.RequestDoctorDto;
import com.Hospital.Management.System.Dto.RequestNurseDto;
import com.Hospital.Management.System.entity.Doctor;
import com.Hospital.Management.System.entity.Nurse;
import com.Hospital.Management.System.entity.RequestDoctor;
import com.Hospital.Management.System.entity.RequestNurse;
import com.Hospital.Management.System.repository.DoctorRepository;
import com.Hospital.Management.System.repository.NurseRepository;
import com.Hospital.Management.System.repository.RequestDoctorRepository;
import com.Hospital.Management.System.repository.RequestNurseRepository;
import org.hibernate.boot.model.source.spi.SingularAttributeSourceToOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RequestController {


    @Autowired
    NurseRepository nurseRepository;

    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    RequestNurseRepository requestNurseRepository;

    @Autowired
    RequestDoctorRepository requestDoctorRepository;

    @PostMapping("/doctor/doctorRequest")
    public String requestNurse(@RequestBody RequestDoctorDto requestDoctorDto)
    {

        int newNurseId=requestDoctorDto.getNewNurseId();
        System.out.println(newNurseId);
        Nurse nurse=nurseRepository.findById(newNurseId).get();
        Doctor doctor=doctorRepository.findById(requestDoctorDto.getDoctorId()).get();
        if(nurse.getNurseStatus().equals("NO") && !nurse.isNurseIsDeleted())
        {
            RequestDoctor requestDoctor=new RequestDoctor();
            requestDoctor.setRequestDoctorId(requestDoctorDto.getDoctorId());
            requestDoctor.setOldNurseId(requestDoctorDto.getOldNurseId());
            requestDoctor.setNewNurseId(requestDoctorDto.getNewNurseId());
            requestDoctor.setDoctor(doctor);
            requestDoctor.setRequestDoctorStatus("pending");

            requestDoctorRepository.save(requestDoctor);
            return "Request for nurse is not added";

        }

       return "Nurse is allocated to someone or new nurse is deleted";
//

    }


    @PostMapping("/nurse/nurseRequest")
    public String requestDoctor(@RequestBody RequestNurseDto requestNurseDto)
    {

        int nurseId= requestNurseDto.getNurseId();
        int oldDoctorId= requestNurseDto.getOldDoctorId();
        int newDoctorId= requestNurseDto.getNewDoctorId();

        Nurse nurse=nurseRepository.findById(nurseId).get();
        if(!nurse.isNurseIsDeleted()) {
            RequestNurse requestNurse = new RequestNurse();
            requestNurse.setNurse(nurse);
            requestNurse.setOldDoctorId(oldDoctorId);
            requestNurse.setNewDoctorId(newDoctorId);

            requestNurseRepository.save(requestNurse);

            return "Request for doctor is added";
        }

        return "Either nurse is allocated or doctor is deleted!!Request not added";

    }
}
