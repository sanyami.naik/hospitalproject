package com.Hospital.Management.System.controller;


import com.Hospital.Management.System.Dto.DoctorDto;
import com.Hospital.Management.System.entity.Nurse;
import com.Hospital.Management.System.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/doctor")
@CrossOrigin
public class DoctorController {

    @Autowired
    private AdminService adminService;

    @PostMapping("/registerDoctor")
    public void registerDoctor(@RequestBody DoctorDto doctorDto) {
        adminService.addDoctor(doctorDto);
    }

    @GetMapping("/{doctorId}")
    public Nurse getDetails(@PathVariable int doctorId)
    {
        return adminService.getDoctorDetails(doctorId);
    }

    @PutMapping("/{doctorId}")
    public void updateDoctor(@PathVariable int doctorId,@RequestBody DoctorDto doctorDto) {
        adminService.updateDoctor(doctorId,doctorDto);
    }


    @DeleteMapping("/{doctorId}")
    public void deleteDoctor(@PathVariable int doctorId)
    {
        adminService.deleteDoctor(doctorId);
    }




}



