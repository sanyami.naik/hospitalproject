package com.Hospital.Management.System.controller;

import com.Hospital.Management.System.Dto.ForgetPasswordDto;
import com.Hospital.Management.System.Dto.ResetPasswordDto;
import com.Hospital.Management.System.entity.*;
import com.Hospital.Management.System.helper.LoginRequestDto;
import com.Hospital.Management.System.helper.LoginResponseDto;
import com.Hospital.Management.System.service.AssignService;
import com.Hospital.Management.System.repository.DoctorRepository;
import com.Hospital.Management.System.repository.NurseRepository;
import com.Hospital.Management.System.security.JwtTokenProvider;
import com.Hospital.Management.System.service.PasswordService;
import org.hibernate.boot.model.source.spi.SingularAttributeSourceToOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin
public class AuthController {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private DoctorRepository doctorPojoRepository;
    @Autowired
    private NurseRepository nursePojoRepository;

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private AssignService assignService;

    @Autowired
    private AuthenticationManager authenticationManager;//this is a inbuilt interface so you cant write @service over it so u will have to make its bean here


    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> autheticateUser(@RequestBody LoginRequestDto loginDto) {


            Authentication authentication = authenticationManager.authenticate(new
                    UsernamePasswordAuthenticationToken(loginDto.getUserName(), loginDto.getUserPassword().trim()));

            SecurityContextHolder.getContext().setAuthentication(authentication);

            String token = jwtTokenProvider.generateToken(authentication);
            int id=jwtTokenProvider.getUserId(jwtTokenProvider.getUsernameFromJWT(token).trim());
            String role=jwtTokenProvider.getRole(jwtTokenProvider.getUsernameFromJWT(token).trim());

            System.out.println(id);
            System.out.println(role);

            System.out.println(token);
            if(token!=null)
                return ResponseEntity.ok(new LoginResponseDto(token,id,role));
            else
                return new ResponseEntity("User name not found",HttpStatus.NOT_FOUND);



        }





    @PostMapping("/forgetPassword")
    public String forgetPassword(@RequestBody ForgetPasswordDto forgetPasswordDto)
    {
        passwordService.forgetPassword(forgetPasswordDto);
        return "Mail send for forget pasword";

    }

    @PostMapping("/resetPassword")
    public String resetPassword(@RequestBody ResetPasswordDto resetPasswordDto)
    {
        passwordService.resetPassword(resetPasswordDto);
        return "Password reset done";

    }






}

