package com.Hospital.Management.System.controller;

import com.Hospital.Management.System.entity.Nurse;
import com.Hospital.Management.System.Dto.NurseDto;
import com.Hospital.Management.System.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController

@RequestMapping("/api/admin/nurse")
@CrossOrigin
public class NurseController {
    @Autowired
    private AdminService adminService;

    @GetMapping("/{nurseId}")
    public Nurse getDetails(@PathVariable int nurseId)
    {
        return adminService.getNurseDetails(nurseId);
    }

    @PostMapping("/registerNurse")
    public void registerNurse(@RequestBody NurseDto nurseDto) {
        adminService.addNurse(nurseDto);
    }

    @PutMapping("/{nurseId}")
    public void updateNurse(@PathVariable int nurseId, @RequestBody NurseDto nurseDto)
    {
        adminService.updateNurse(nurseId,nurseDto);
    }

    @DeleteMapping("/{nurseId}")
    public void deleteNurse(@PathVariable int nurseId)
    {
        adminService.deleteNurse(nurseId);
    }
}
