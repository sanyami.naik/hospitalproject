package com.Hospital.Management.System.entity;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

        @Entity
        @Data
        @Table(uniqueConstraints=@UniqueConstraint(columnNames="userPassword"))
        @NoArgsConstructor

        public class User {

            @Id
            @GeneratedValue(strategy = GenerationType.IDENTITY)
            private long userId;
            private String userName;
            private String userPassword;

            @ManyToMany
            @JoinTable(
                    name = "User_Role",
                    joinColumns = { @JoinColumn(name = "user_id",referencedColumnName = "userId") },
                    inverseJoinColumns = { @JoinColumn(name = "role_id",referencedColumnName = "roleId") }
            )
            private Set<Role> rolesSet ;

            public User(String userName, String userPassword) {
                this.userName = userName;
                this.userPassword = userPassword;
            }
        }
