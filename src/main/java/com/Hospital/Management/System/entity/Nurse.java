package com.Hospital.Management.System.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "nurseEmail")})
public class Nurse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int nurseId;
    private int nurseYearsOfExp;
    private String nurseName;
    private String nurseEmail;
    private String nurseStatus;
    private String nursePassword;
    private boolean nurseIsDeleted;


    @Override
    public String toString() {
        return "Nurse{" +
                "nurseId=" + nurseId +
                ", nurseYearsOfExp=" + nurseYearsOfExp +
                ", nurseName='" + nurseName + '\'' +
                ", nurseEmail='" + nurseEmail + '\'' +
                ", nurseStatus='" + nurseStatus + '\'' +
                ", nursePassword='" + nursePassword + '\'' +
                ", nurseIsDeleted=" + nurseIsDeleted +
                '}';
    }

    @JsonBackReference
    @ManyToOne
    private Doctor doctor;


    @JsonManagedReference
    @OneToMany(mappedBy = "nurse")
    private List<RequestNurse> requestNurses;

    public Nurse(String nursePassword,int nurseYearsOfExp, String nurseName, String nurseEmail,String nurseStatus) {
        this.nurseStatus=nurseStatus;
        this.nursePassword=nursePassword;
        this.nurseYearsOfExp = nurseYearsOfExp;
        this.nurseName = nurseName;
        this.nurseEmail = nurseEmail;
    }
}
