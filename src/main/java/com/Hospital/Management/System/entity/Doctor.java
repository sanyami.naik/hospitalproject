package com.Hospital.Management.System.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = "doctorEmail")})
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int doctorId;
    private String doctorPassword;
    private String doctorName;
    private String doctorEmail;

    @Override
    public String toString() {
        return "Doctor{" +
                "doctorId=" + doctorId +
                ", doctorName='" + doctorName + '\'' +
                ", doctorEmail='" + doctorEmail + '\'' +
                ", doctorSpecialization='" + doctorSpecialization + '\'' +
                ", doctorIsDeleted=" + doctorIsDeleted +
                '}';
    }

    private String doctorSpecialization;
    private boolean doctorIsDeleted;

    @JsonManagedReference
    @OneToMany(mappedBy = "doctor")
    private List<Nurse> nurseList;

    @JsonManagedReference
    @OneToMany(mappedBy = "doctor")
    private List<RequestDoctor> requestDoctors;

    public Doctor(String doctorPassword,String doctorName, String doctorEmail, String doctorSpecialization) {
        this.doctorPassword=doctorPassword;
        this.doctorName = doctorName;
        this.doctorEmail = doctorEmail;
        this.doctorSpecialization = doctorSpecialization;
    }
}
