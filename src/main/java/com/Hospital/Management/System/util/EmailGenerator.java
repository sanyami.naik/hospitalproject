package com.Hospital.Management.System.util;

import com.Hospital.Management.System.service.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Random;

@Service
public class EmailGenerator {

    @Autowired
    private EmailSenderService emailSenderService;

    public String generatePassword(String userName)
    {
        Random random=new Random();
        int num=random.nextInt(1000000);
        String newPassword=String.valueOf(num);
        emailSenderService.sendSimpleEmail(userName, "Hii you have successfully registered .Your password is "+newPassword+" .Keeep it safee you!!!","Forgot Password");
        return newPassword;
    }
}
