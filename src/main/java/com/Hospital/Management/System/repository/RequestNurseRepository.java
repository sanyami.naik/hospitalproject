package com.Hospital.Management.System.repository;

import com.Hospital.Management.System.entity.RequestNurse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestNurseRepository extends JpaRepository<RequestNurse,Integer> {
}
