package com.Hospital.Management.System.repository;

import com.Hospital.Management.System.entity.Role;
import com.Hospital.Management.System.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Integer> {

    Optional<Role> findByRoleName(String userName);
}
