package com.Hospital.Management.System.repository;

import com.Hospital.Management.System.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Integer> {

    //Optional<UserPojo> findByEmail(String email);
    Optional<User> findByUserName(String userName);
//    Boolean existsByUserName(String username);
//    Boolean existsByEmail(String email);
}
