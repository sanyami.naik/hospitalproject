package com.Hospital.Management.System.repository;

import com.Hospital.Management.System.entity.Nurse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NurseRepository extends JpaRepository<Nurse,Integer> {


    Optional<Nurse> findByNurseEmail(String nursePojoEmail);
}
