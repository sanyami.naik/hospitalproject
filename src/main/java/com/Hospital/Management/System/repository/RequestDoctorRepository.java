package com.Hospital.Management.System.repository;

import com.Hospital.Management.System.entity.RequestDoctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestDoctorRepository extends JpaRepository<RequestDoctor,Integer> {
}
