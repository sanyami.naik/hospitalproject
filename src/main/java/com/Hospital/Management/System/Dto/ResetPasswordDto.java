package com.Hospital.Management.System.Dto;

import lombok.Data;

@Data
public class ResetPasswordDto {

    private String userName;
    private String oldPassword;
    private String newPassword;

}
