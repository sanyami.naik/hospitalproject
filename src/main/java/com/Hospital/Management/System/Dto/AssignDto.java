package com.Hospital.Management.System.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AssignDto {

    private int doctorId;
    private int nurseId;
}
