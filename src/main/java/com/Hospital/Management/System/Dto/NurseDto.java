package com.Hospital.Management.System.Dto;



import com.Hospital.Management.System.entity.Doctor;
import lombok.Data;

@Data
public class NurseDto {

    private int nurseId;

    private String userName;
    private String nursePassword;
    private String nurseName;
    private int nurseYearsOfExperience;
    private String isAllocated;
    private boolean isDeleted;
    private Doctor doctor;

    public NurseDto(int nurseId, String userName, String nurseName, int nurseYearsOfExperience) {
        this.nurseId = nurseId;
        this.userName = userName;
        this.nurseName = nurseName;
        this.nurseYearsOfExperience = nurseYearsOfExperience;
    }

    @Override
    public String toString() {
        return "NurseDto{" +
                "nurseId=" + nurseId +
                ", userName='" + userName + '\'' +
                ", nurseName='" + nurseName + '\'' +
                ", nurseYearsOfExperience=" + nurseYearsOfExperience +
                ", isAllocated='" + isAllocated + '\'' +
                ", doctor=" + doctor +
                '}';
    }
}





