package com.Hospital.Management.System.Dto;

import lombok.Data;

@Data
public class RequestDoctorDto {
    private int doctorId;
    private int oldNurseId;
    private int newNurseId;

}