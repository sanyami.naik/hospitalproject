package com.Hospital.Management.System.Dto;

import lombok.Data;

@Data
public class RequestNurseDto {

    private int nurseId;
    private int oldDoctorId;
    private int newDoctorId;

}
