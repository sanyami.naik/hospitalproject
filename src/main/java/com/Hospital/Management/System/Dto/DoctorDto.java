package com.Hospital.Management.System.Dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DoctorDto {

    private int doctorId;
    private String userName;      //email

    private String doctorPassword;
    private String doctorName;
    private String doctorSpecialization;

    private boolean isDeleted;

   // private List<Nurse> nurseList;

    public DoctorDto(String userName, String doctorName, String doctorSpecialization) {
        this.userName = userName;
        this.doctorName = doctorName;
        this.doctorSpecialization = doctorSpecialization;
        //this.nurseList=nurseList;

    }
    public DoctorDto(int doctorId,String userName, String doctorName, String doctorSpecialization) {
        this.doctorId=doctorId;
        this.userName = userName;
        this.doctorName = doctorName;
        this.doctorSpecialization = doctorSpecialization;
        //this.nurseList=nurseList;

    }


}
