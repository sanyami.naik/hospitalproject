package com.Hospital.Management.System.security;

import com.Hospital.Management.System.entity.Doctor;
import com.Hospital.Management.System.entity.Nurse;
import com.Hospital.Management.System.entity.Role;
import com.Hospital.Management.System.entity.User;
import com.Hospital.Management.System.exception.BlogApiException;
import com.Hospital.Management.System.repository.DoctorRepository;
import com.Hospital.Management.System.repository.NurseRepository;
import com.Hospital.Management.System.repository.UserRepository;
import io.jsonwebtoken.*;
import net.bytebuddy.utility.nullability.AlwaysNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;

@Service
public class JwtTokenProvider {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    NurseRepository nurseRepository;

    @Value("${app.jwt-secret}")
    private String jwtSecretKey;
    @Value("${app.jwt-expiration-milliseconds}")
    private int jwtExpirationInMs;

    // generate token method

    public String generateToken(Authentication authentication) {
        System.out.println("+++++++++++++");
        String username = authentication.getName();
        Date currentDate = new Date();
        Date expireDate = new Date(currentDate.getTime() + jwtExpirationInMs);

        System.out.println("++++++++++++");
        String token = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512,jwtSecretKey)
                .compact();

        System.out.println("+++++++++++");
        return token;
    }

    // get username from token

    public String getUsernameFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecretKey)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();

    }


    public String getRole(String username){
        User user=userRepository.findByUserName(username).get();
        Set<Role> roleSet= user.getRolesSet();
        String role = null;
        for (Role r :
                roleSet) {
            role=r.getRoleName();
            break;
        }
        return role;
    }



    public int getUserId(String username){
        String roleOfUser=getRole(username);
        if(roleOfUser.equals("ADMIN_ROLE"))
        {
            return 1;
        } else if (roleOfUser.equals("DOCTOR_ROLE"))
        {
            Doctor doctor =doctorRepository.findByDoctorEmail(username).get();
            return doctor.getDoctorId();
        }else
        {
            Nurse nurse=nurseRepository.findByNurseEmail(username).get();
            return nurse.getNurseId();
        }

    }


     //Validate jwt token
    public boolean validateToken(String token)  {
        try{
            Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token);
            return true;
        } catch (SignatureException exception) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST,"Invalid Jwt Signature");
        } catch (MalformedJwtException exception) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST,"Invalid Jwt token");
        } catch (ExpiredJwtException exception) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST,"Expired JWT Token");
        } catch (UnsupportedJwtException exception) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST,"Unsupported JWT token");
        } catch (IllegalArgumentException exception) {
            throw new BlogApiException(HttpStatus.BAD_REQUEST,"JWT claims string is empty");
        }

    }
}
