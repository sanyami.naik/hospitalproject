package com.Hospital.Management.System.service;

import com.Hospital.Management.System.Dto.ForgetPasswordDto;
import com.Hospital.Management.System.Dto.ResetPasswordDto;
import com.Hospital.Management.System.entity.*;
import com.Hospital.Management.System.helper.PasswordEncoding;
import com.Hospital.Management.System.repository.DoctorRepository;
import com.Hospital.Management.System.repository.NurseRepository;
import com.Hospital.Management.System.repository.UserRepository;
import com.Hospital.Management.System.util.EmailGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {

    @Autowired
    private EmailGenerator emailGenerator;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private NurseRepository nurseRepository;
    public void resetPassword(ResetPasswordDto resetPasswordDto) {

        User user=userRepository.findByUserName(resetPasswordDto.getUserName().trim()).get();
        if(user!=null) {

               Doctor doctor=doctorRepository.findByDoctorEmail(resetPasswordDto.getUserName()).get();
               if(doctor!=null)
               {
                 if (doctor.getDoctorPassword().trim().equals(resetPasswordDto.getOldPassword().trim()))
                 {

                    user.setUserPassword(PasswordEncoding.passwordEncrypter(resetPasswordDto.getNewPassword().trim()));
                    userRepository.save(user);
                    doctor.setDoctorPassword(doctor.getDoctorPassword().trim());
                    doctorRepository.save(doctor);
                    System.out.println(user.getUserPassword());
                 }
               }

            else
            {
                Nurse nurse=nurseRepository.findByNurseEmail(resetPasswordDto.getUserName().trim()).get();
                if(nurse!=null) {
                    if (nurse.getNursePassword().trim().equals(resetPasswordDto.getOldPassword())) {

                        user.setUserPassword(PasswordEncoding.passwordEncrypter(resetPasswordDto.getNewPassword().trim()));
                        userRepository.save(user);
                        nurse.setNursePassword(nurse.getNursePassword().trim());
                        nurseRepository.save(nurse);
                    }
                }

            }
        }

    }

    public void forgetPassword(ForgetPasswordDto forgetPasswordDto) {

        User user=userRepository.findByUserName(forgetPasswordDto.getUserName()).get();
        if(user!=null) {
            String password = emailGenerator.generatePassword(forgetPasswordDto.getUserName());

            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String newPass = passwordEncoder.encode(password);
            user.setUserPassword(newPass);

            userRepository.save(user);
        }
        else {
            System.out.println("no user present");
        }

    }
}
