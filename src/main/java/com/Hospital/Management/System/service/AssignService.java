package com.Hospital.Management.System.service;


import com.Hospital.Management.System.Dto.AssignDto;
import com.Hospital.Management.System.entity.Doctor;
import com.Hospital.Management.System.entity.Nurse;
import com.Hospital.Management.System.repository.DoctorRepository;
import com.Hospital.Management.System.repository.NurseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class AssignService {

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private NurseRepository nurseRepository;

    public String assignDoctor( AssignDto assignDto)
    {

        Nurse nurse=nurseRepository.findById(assignDto.getNurseId()).get();
        Doctor doctor=doctorRepository.findById(assignDto.getDoctorId()).get();

        if(nurse.getNurseStatus().equals("NO")) {

            nurse.setDoctor(doctor);
            nurse.setNurseStatus("YES");

            doctorRepository.save(doctor);
            nurseRepository.save(nurse);
            return "doctor assigned!!";
        }
        else
            return "nurse already has a doctor";
    }

    public String assignNurse(AssignDto assignDto) {
        Nurse nurse=nurseRepository.findById(assignDto.getNurseId()).get();
        Doctor doctor=doctorRepository.findById(assignDto.getDoctorId()).get();

        List<Nurse> nurseList=doctor.getNurseList();
        nurseList.add(nurse);

        nurse.setNurseStatus("YES");

        doctorRepository.save(doctor);
        nurseRepository.save(nurse);


        return "nurse assigned!!";
    }
}
