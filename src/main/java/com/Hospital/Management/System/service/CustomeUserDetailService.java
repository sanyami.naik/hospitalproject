package com.Hospital.Management.System.service;


import com.Hospital.Management.System.entity.Role;
import com.Hospital.Management.System.entity.User;
import com.Hospital.Management.System.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
@Service
@Configuration
public class CustomeUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userPojoRepository;

    public CustomeUserDetailService(UserRepository userPojoRepository) {

        this.userPojoRepository = userPojoRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User userPojo = userPojoRepository.findByUserName(username)
                .orElseThrow(()-> new UsernameNotFoundException("User not found with username or email"+username));

        return  new org.springframework.security.core.userdetails.User(userPojo.getUserName(),userPojo.getUserPassword(),
                mapRolesToAuthorities(userPojo.getRolesSet()));
    }

    private Collection< ? extends GrantedAuthority> mapRolesToAuthorities(Set<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
    }

}