package com.Hospital.Management.System.service;

import com.Hospital.Management.System.Dto.DoctorDto;
import com.Hospital.Management.System.Dto.NurseDto;
import com.Hospital.Management.System.entity.*;
import com.Hospital.Management.System.repository.DoctorRepository;
import com.Hospital.Management.System.repository.NurseRepository;
import com.Hospital.Management.System.repository.RoleRepository;
import com.Hospital.Management.System.repository.UserRepository;
import com.Hospital.Management.System.util.EmailGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AdminService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private NurseRepository nurseRepository;

    @Autowired
    private EmailGenerator emailGenerator;

    @Autowired
    private RoleRepository roleRepository;

    public Doctor addDoctor(DoctorDto doctorDto) {
        String uName = doctorDto.getUserName().trim();
        System.out.println(uName);
        String password = emailGenerator.generatePassword(uName);

        System.out.println(password);
        System.out.println(doctorDto.getDoctorSpecialization());
        System.out.println(doctorDto.getDoctorName());
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String newPass = passwordEncoder.encode(password.trim());
        User user = new User(doctorDto.getUserName().trim(), newPass);


        Role role = roleRepository.findByRoleName("DOCTOR_ROLE").get();
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);
        user.setRolesSet(roleSet);


        Doctor doctor = new Doctor(password, doctorDto.getDoctorName(), doctorDto.getUserName(), doctorDto.getDoctorSpecialization());

        userRepository.save(user);
        doctorRepository.save(doctor);

        return doctor;
    }

    public Nurse addNurse(NurseDto nurseDto) {
        String uName = nurseDto.getUserName();
        System.out.println(uName);

        String password = emailGenerator.generatePassword(uName.trim());

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String newPass = passwordEncoder.encode(password);

        User user = new User(nurseDto.getUserName(), newPass);
        Nurse nurse = new Nurse(password, nurseDto.getNurseYearsOfExperience(), nurseDto.getNurseName(), nurseDto.getUserName().trim()
                , "NO");


        Role role = roleRepository.findByRoleName("NURSE_ROLE").get();
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);
        user.setRolesSet(roleSet);


        nurseRepository.save(nurse);
        userRepository.save(user);

        return nurse;
    }


    public Doctor updateDoctor(long doctorId, DoctorDto doctorDto) {

        Doctor doctor = doctorRepository.findByDoctorEmail(doctorDto.getUserName()).get();
        doctor.setDoctorName(doctorDto.getDoctorName());
        doctor.setDoctorSpecialization(doctorDto.getDoctorSpecialization());


        doctorRepository.save(doctor);
        return doctor;
    }

    public Nurse updateNurse(long nurseId, NurseDto nurseDto) {

        Nurse nurse = nurseRepository.findByNurseEmail(nurseDto.getUserName()).get();
        nurse.setNurseName(nurseDto.getNurseName());
        nurse.setNurseYearsOfExp(nurseDto.getNurseYearsOfExperience());

        nurseRepository.save(nurse);
        return nurse;
    }


    public void deleteDoctor(int doctorId) {

        Doctor doctor = doctorRepository.findById(doctorId).get();
        doctor.setDoctorIsDeleted(true);
        doctorRepository.save(doctor);
    }

    public void deleteNurse(int nurseId) {

        Nurse nurse = nurseRepository.findById(nurseId).get();
        nurse.setNurseIsDeleted(true);
        nurseRepository.save(nurse);

    }

    public Nurse getNurseDetails(int nurseId) {

        Nurse nurse = nurseRepository.findById(nurseId).get();
        return nurse;
    }

    public Nurse getDoctorDetails(int doctorId) {
        Nurse nurse = nurseRepository.findById(doctorId).get();
        return nurse;
    }


    public List<Nurse> showAllNurses() {
        return nurseRepository.findAll();
    }

    public List<Doctor> showAllDoctors() {
        return doctorRepository.findAll();

    }
}